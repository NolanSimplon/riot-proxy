const express = require('express');
const bodyParser = require('body-parser');
const req = require('request');

const app = express();
const cors = require('cors');


app.use(bodyParser.json());
app.use(cors());
app.post('/', function(request, response) {
    console.log(request.body)
    req(request.body.url, function(error, res, body) {
        console.log(body);
        response.setHeader('Content-Type', 'application/json');
        response.send(body);
    });
});


app.listen('3000');